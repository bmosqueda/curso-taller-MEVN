function middleware(req, res, next) {
  if(req.session.idUsuario) {
    //Si entra es porque ya inició sesión y le pasamos el conrtol al siguiente middleware
    next();
  }
  else {
    res.redirect("/login");
    //Si no ha iniciado sesión "cortamos" la petición respondiéndole que tiene que logguearse
  }
}

//Exportamos la función middleware
module.exports = middleware;