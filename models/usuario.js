const mongoose = require('mongoose')
let Schema = mongoose.Schema;
const emailMatch = [/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i, "This text is not an email"]

const usuarioEsquema = new Schema({
  nombre: {
     type: String, 
     required: true, 
     maxlength: [70, "Nombre muy largo"] 
  },
  password: {
    type: String,
    required: true,
    minlength: [ 8, "La contraseña debe de tener por lo menos 8 caracteres" ]
  },
  correo: { 
    type: String, 
    required: true,
    unique: [true, "Este correo ya fue registrado"], 
    match: emailMatch 
  }
});

let Usuario = mongoose.model('usuarios', usuarioEsquema);
module.exports.Usuario = Usuario; 