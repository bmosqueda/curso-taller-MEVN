//Requerimos mongoose
const mongoose = require('mongoose')
//Creamos una varible esquema que hereda de mogoose schema 
let Schema = mongoose.Schema;

/*Instanciamos un nuevo schema pasándole como parametro un objeto con las propiedas
de cada campo del esquema*/
const blogSchema = new Schema({
  titulo: {
    type: String,   //Tipo de dato
    required: true,   //Este campo es requerido
    maxlength: [50, "Título demasiado largo"] //Máximo 50 caracteres, de lo contrario muestra el texto de error que le enviamos
  },  
  contenido: {
    type: String, 
    required: [true, "El campo contenido es requerido"],  //Aquí también podemos definir un texto de error 
    maxlength: 250  
  },
  fechaCreacion: {
    type: Date,   
    default: new Date(),  //Fecha en formato ISOString Ej. 2018-08-10T02:25:24.362Z
    required: true
  },
  comentarios: {
    type: Array,
    default: []   //El valor por default en caso de no asignárselo es un arreglo vacío
  },
  idCreador: {
    type: Schema.Types.ObjectId, 
    ref: "usuarios",
    required: true 
  }
});

/*Tipos de datos aceptados por mongoose
  -String
  -Number
  -Date
  -Buffer
  -Boolean
  -Mixed
  -ObjectId   (que hace referencia al id de otro esquema)
  -Array
  -Decimal128
  -Map
*/

//Para más información acerca de los esquemas http://mongoosejs.com/docs/guide.html

let Blog = mongoose.model('Blog', blogSchema);  //Creamos el modelo pasándole el nombre que le vamos a poner y el esquema
module.exports.Blog = Blog; //Exportamos el modelo para poder usarlo en otros archivos del proyecto