global.__PORT = 3000;
global.__URL = 'http://127.0.0.1';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const sessionMiddleware = require('./session-middleware');
const routerSession = require('./routes/session');

//Para el manejo de sesiones
const session = require('express-session'); // npm install --save express-session

app.use(bodyParser.json()); //Para peticiones aplication/json
app.use(bodyParser.urlencoded({extended: true}));

//https://www.npmjs.com/package/express-session   Documentación
app.use(session({
  secret: 'cualquier cosa',
  resave: true,
  saveUninitialized: 1
}));

mongoose.connect(
  //Cadena de conección a MongoDB en el formato
  //'mongodb://usuario:contraseña@host:puerto/nombreBaseDeDatos'
  'mongodb://curso-MEVN-user:cursoPassword@localhost:27017/Curso-MEVN', 
  {useNewUrlParser: true},
  function(error) {
    if(error)
      console.error('Hubo un error al conectarse con MongoDB ', error);
    else
      console.log('Conección existosa a MongoDB');
  }
);

app.use('/home', sessionMiddleware);


app.get('/registro', function(req, res) {
  if(req.session.idUsuario) 
    res.redirect('/home');
  else
    res.render('registro.pug');
});

app.get('/login', function(req, res) {
  if(req.session.idUsuario) 
    res.redirect('/home');
  else
    res.render('login.pug');
});

app.get('/home', function(req, res) {
  res.render('index.pug');
});

//Requerimos el router que creamos en la carpeta de api
const blogRouter = require('./api/blog.js');

app.use('/api/blog', blogRouter);
app.use('/sesion', routerSession);

app.set("view engine", "pug");
app.use('/', express.static(__dirname + "/node_modules/bulma/css/"));

app.listen(__PORT, function() {
  console.log('Servidor funcionando :D');
});
