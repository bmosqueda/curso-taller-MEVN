const router = require('express').Router();
const Blog = require('../models/blog.js').Blog;
const bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.route('/')
  .get(function(req, res) {
    let filters = {};
    let options = {};

    if(req.query.titulo)
      filters.titulo = req.query.titulo;

    if(req.query.fechaCreacion)
      filters.fechaCreacion = req.query.fechaCreacion;

    //Le pasamos los filtros que recibimos del usuario como un objeto, 
    //Si no trae nada se le envía el objeto vacío y devuelve todos
    let query = Blog.find(filters);

    //Parámetros para poder paginar los datos de nuestra API
    if(!isNaN(req.query.limite))
      query.limit(parseInt(req.query.limite));
    if(!isNaN(req.query.despues))
      query.skip(parseInt(req.query.despues));

    query.exec(function(error, blogs) {
      if(error) {
        console.error('Error al buscar los blogs');
        console.error(error);
        res.status(500).json({error: error});
      }
      else {
        res.json(blogs);
      }
    });
  })
  .post(function(req, res) {
    //Creamos el nuevo blog con una nueva instancia del modelo
    if(!req.session.idUsuario) {
      res.sendStatus(401);
      return;
    }

    let blog = new Blog({
      titulo: req.body.titulo,
      contenido: req.body.contenido,
      idCreador: req.session.idUsuario,
      comentarios: req.body.comentarios || []
    })

    //Lo guardamos
    blog.save(function(error) {
      if(error) {
        console.error('Error al guardar el blog');
        console.error(error);
        res.status(500).json({error: error});
      }
      else {
        res.json(blog);
      }
    });
  });

router.route('/:id')
  .get(function(req, res) {
    //Búsqueda únicamente por el id (que viene como parte de la ruta)
    Blog.find({_id: req.params.id})
      .populate('idCreador')
      .exec()
      .then(function(error, blog) {
        if(error) {
          console.error('Error al buscar el blog');
          console.error(error);
          res.status(500).json({error: error});
        }
        else {
          res.json(blog);
        }
      })
      .catch(err => {
        console.log(err.message);
        res.json({error: err.message});
      })
  })
  .put(function(req, res) {
    Blog.findByIdAndUpdate(
      req.params.id, 
      {
        $set: req.body
      },
      {new: true},  //Sin esta línea devulve la información del blog antes de actualizarse
      function(error, blog) {
        console.log(blog);
        if(error) {
          console.error("Hubo un error al actualizar el blog con el id: ", req.params.id);
          console.error(error);
          res.status(500).json({error: error});
        }
        //O podemos devolver solamente un status 204 (Success no content)
        res.json(blog);
      }
    )
  })
  .delete(function(req, res) {
    Blog.findByIdAndRemove(req.params.id, function(error, blog) {
      console.log(blog);
      if(error) {
        console.error("Hubo un error al eliminar el blog con el id: ", req.params.id);
        console.error(error);
        res.status(500).json({error: error});
      }
      //O podemos devolver solamente un status 204 (Success no content)
      res.json(blog);
    });
  })

//Exportamos el router para poder requerirlo en otros archivos.
module.exports = router;