const router = require('express').Router();
const Usuario = require('../models/usuario').Usuario;

router.post('/entrar', function(req, res) {
  var user = req.body;
  console.log(user);
  if(!user)
    res.sendStatus(400);

  Usuario.find({correo: user.correo}, function(error, usuario) {
    if(error) {
      console.error(error.message);
      res.sendStatus(500);
      return;
    }

    console.log(usuario);
    if(usuario[0] === undefined) {
      res.status(400).json({message: 'No hay ningún usuario con ese correo'});
      return;
    }

    if(usuario[0].password === user.password) {
      req.session.idUsuario = usuario[0]._id; 
      req.session.nombre = usuario[0].nombre; 
      req.session.save();

      res.sendStatus(200); 
    }
    else
      res.sendStatus(401);  //Unauthorized 
  });
});

router.post('/registrarse', function(req, res) {
  var usuario = new Usuario(req.body);

  usuario.save(function(error) {
    if(error) {
      console.error(error.message);
      res.status(400).json({error: error});
    }
    else {
      res.json(usuario);      
    }
  });
});

router.get('/logout', function(req, res) {
  if(req.session.idUsuario) {
    req.session.destroy();
    req.session = null; 

    res.send(200);
  }
  else {
    res.send(400);
  }
});

module.exports = router;